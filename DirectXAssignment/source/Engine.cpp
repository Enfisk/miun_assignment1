#pragma once
#include "Engine.h"
#include "WindowManager.h"
#include "GraphicsEngine.h"
#include "GameEngine.h"
#include "Input.h"

Engine::Engine(HINSTANCE p_instance) {
	m_windowManager = new WindowManager(p_instance);
	if (!WindowManager::GetWindowHandle()) {
		m_running = false;
		return;
	}

	m_graphics = new GraphicsEngine();
	m_gameEngine = new GameEngine(m_graphics);
}

Engine::~Engine() {
	if (m_graphics != nullptr)
		delete m_graphics;

	if (m_windowManager != nullptr)
		delete m_windowManager;

	if (m_gameEngine != nullptr)
		delete m_gameEngine;
}

void Engine::Run() {
	while (m_running) {
		PollEvents();
		Input::Update();
		m_gameEngine->Update();
		m_graphics->Draw();
		::Sleep(m_sleepDuration);		//No regrets.
	}
}

void Engine::PollEvents() {
	MSG msg = { 0 };
	if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
		switch (msg.message) {
			case WM_QUIT:
			{
				m_running = false;
				break;
			}

			case WM_SETFOCUS:
			{
				m_sleepDuration = 10;
				break;
			}

			case WM_KILLFOCUS:
			{
				m_sleepDuration = 100;
				break;
			}
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}