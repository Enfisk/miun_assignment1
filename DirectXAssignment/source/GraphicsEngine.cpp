#pragma once
#include "GraphicsEngine.h"
#include <string>
#include <map>
#include "Utilities.h"
#include "WindowManager.h"
#include "Texture.h"
#include "SpriteBatch.h"
#include "CommonStates.h"
#include "Sprite.h"

GraphicsEngine::GraphicsEngine() {
	CreateDeviceAndSwapChain();
	CreateRenderTargetView();
	CreateDepthStencil();
	CreateViewport();

	m_spriteBatch = new DirectX::SpriteBatch(m_deviceContext);
	m_states = new DirectX::CommonStates(m_device);
}

GraphicsEngine::~GraphicsEngine() {
	{
		auto it = m_textures.begin();
		while (it != m_textures.end()) {
			delete it->second;
			++it;
		}

		m_textures.clear();
	}

	if (m_device) {
		m_device->Release();
		m_device = nullptr;
	}

	if (m_deviceContext) {
		m_deviceContext->ClearState();
	}

	if (m_swapChain) {
		m_swapChain->Release();
		m_swapChain = nullptr;
	}

	if (m_renderTargetView) {
		m_renderTargetView->Release();
		m_renderTargetView = nullptr;
	}

	if (m_depthStencilBuffer) {
		m_depthStencilBuffer->Release();
		m_depthStencilBuffer = nullptr;
	}

	if (m_depthStencilView) {
		m_depthStencilView->Release();
		m_depthStencilView = nullptr;
	}

	delete m_states;
	delete m_spriteBatch;
}

void GraphicsEngine::Draw() {
	m_deviceContext->ClearRenderTargetView(m_renderTargetView, DirectX::Colors::Orchid);
	m_deviceContext->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	m_spriteBatch->Begin(DirectX::SpriteSortMode_FrontToBack, m_states->NonPremultiplied());

	for (int i = 0; i < m_queuedDrawables.size(); ++i) {
		Sprite* drawable = m_queuedDrawables[i];
		Vector3 position = drawable->GetPosition();		//We're doing a lot of copying here, but w/e
		Vector2 size = drawable->GetSize();

		RECT destRect;
		destRect.left = (long)position.x;
		destRect.top = (long)position.y;
		destRect.right = (long)(position.x + size.x);
		destRect.bottom = (long)(position.y + size.y);
		
		m_spriteBatch->Draw(drawable->GetTexture()->GetShaderResourceView(), destRect, &drawable->GetTectureRect(),
							DirectX::Colors::White, 0.0f, Vector2(0, 0), DirectX::SpriteEffects_None, position.z);
	}

	m_spriteBatch->End();

	HRESULT hr = m_swapChain->Present(0, 0);
	if (FAILED(hr)) {
		std::wstring errorMessage = L"SwapChain->Present failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return;
	}

	m_queuedDrawables.clear();
}

void GraphicsEngine::QueueForDrawing(Sprite* p_drawable) {
	m_queuedDrawables.push_back(p_drawable);
}

Texture* GraphicsEngine::GetTexture(const std::string &p_filePath) {
	auto it = m_textures.find(p_filePath);
	if (it != m_textures.end())
		return it->second;

	Texture* texture = new Texture(p_filePath, m_device);
	m_textures[p_filePath] = texture;
	return texture;
}

bool GraphicsEngine::CreateDeviceAndSwapChain() {
	HRESULT hr;

	D3D_FEATURE_LEVEL levels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};

	UINT flags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferDesc.Width = Utilities::GetRectWidth(WindowManager::GetWindowRect());
	swapChainDesc.BufferDesc.Height = Utilities::GetRectHeight(WindowManager::GetWindowRect());
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = WindowManager::GetWindowHandle();
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, levels, ARRAYSIZE(levels), D3D11_SDK_VERSION, &swapChainDesc, &m_swapChain, &m_device, &m_featureLevel, &m_deviceContext);

	if (FAILED(hr)) {
		std::wstring errorMessage = L"D3D11CreateDeviceAndSwapChain failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return false;
	}

	return true;
}

bool GraphicsEngine::CreateRenderTargetView() {
	HRESULT hr;
	
	ID3D11Texture2D* backBuffer;
	
	hr = m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
	if (FAILED(hr)) {
		std::wstring errorMessage = L"m_swapChain->GetBuffer failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return false;
	}

	backBuffer->GetDesc(&m_backBufferDesc);
	hr = m_device->CreateRenderTargetView(backBuffer, nullptr, &m_renderTargetView);

	if (FAILED(hr)) {
		backBuffer->Release();
		backBuffer = nullptr;

		std::wstring errorMessage = L"m_device->CreateRenderTargetView failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return false;
	}

	backBuffer->Release();
	backBuffer = nullptr;

	return true;
}

bool GraphicsEngine::CreateDepthStencil() {
	HRESULT hr;
	
	D3D11_TEXTURE2D_DESC dsc;
	ZeroMemory(&dsc, sizeof(dsc));

	dsc.Width = Utilities::GetRectWidth(WindowManager::GetWindowRect());
	dsc.Height = Utilities::GetRectHeight(WindowManager::GetWindowRect());
	dsc.MipLevels = 1;
	dsc.ArraySize = 1;
	dsc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	dsc.Usage = D3D11_USAGE_DEFAULT;
	dsc.SampleDesc.Count = 1;
	dsc.SampleDesc.Quality = 0;

	hr = m_device->CreateTexture2D(&dsc, nullptr, &m_depthStencilBuffer);
	if (FAILED(hr)) {
		std::wstring errorMessage = L"CreateDepthStencilBuffer failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return false;
	}

	hr = m_device->CreateDepthStencilView(m_depthStencilBuffer, nullptr, &m_depthStencilView);
	if (FAILED(hr)) {
		std::wstring errorMessage = L"CreateDepthStencilView failed! Error: " + hr;
		MessageBox(0, errorMessage.c_str(), L"Error", MB_OK);
		PostQuitMessage(-1);
		return false;
	}

	return true;
}

void GraphicsEngine::CreateViewport() {
	m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);

	m_viewPort.TopLeftX = 0.0f;
	m_viewPort.TopLeftY = 0.0f;
	m_viewPort.Width = static_cast<FLOAT>(Utilities::GetRectWidth(WindowManager::GetWindowRect()));
	m_viewPort.Height = static_cast<FLOAT>(Utilities::GetRectHeight(WindowManager::GetWindowRect()));
	m_viewPort.MinDepth = 0.0f;
	m_viewPort.MaxDepth = 1.0f;

	m_deviceContext->RSSetViewports(1, &m_viewPort);
}
