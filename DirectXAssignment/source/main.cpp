#include <Windows.h>
#include "Engine.h"

#if defined(DEBUG) || defined(_DEBUG)
#include "vld.h"
#endif

#pragma warning(disable:4100)		//unreferenced formal parameter

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int args) {
	Engine engine(hInstance);
	engine.Run();

	return 0;
}
