#include "EntityManager.h"
#include "Entity.h"
#include "GraphicsEngine.h"

EntityManager::EntityManager() {

}

EntityManager::~EntityManager() {
	auto it = m_entities.begin();
	while (it != m_entities.end()) {
		delete *it;
		++it;
	}

	m_entities.clear();
	m_entitiesToRemove.clear();
}

void EntityManager::UpdateAllEntities() {
	for (int i = 0; i < m_entities.size(); ++i) {
		m_entities[i]->Update();
	}
}

void EntityManager::QueueAllEntitiesForDrawing(GraphicsEngine* p_graphicsEngine) {
	for (int i = 0; i < m_entities.size(); ++i) {
		Sprite* currentSprite = m_entities[i]->GetSprite();
		if (currentSprite == nullptr) continue;

		p_graphicsEngine->QueueForDrawing(currentSprite);
	}
}

void EntityManager::RemoveEntity(Entity* p_entity) {
	m_entitiesToRemove.push_back(p_entity);
}

void EntityManager::DeleteAllQueuedEntities() {
	auto it = m_entitiesToRemove.begin();
	auto removeIt = m_entities.begin();

	while (it != m_entitiesToRemove.end()) {
		removeIt = std::find(m_entities.begin(), m_entities.end(), *it);

		if (removeIt != m_entities.end())
			m_entities.erase(removeIt);

		delete *it;
		++it;
	}

	m_entitiesToRemove.clear();
}