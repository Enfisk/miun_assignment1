#include "Input.h"
#include "Keyboard.h"
#include "Mouse.h"

std::unique_ptr<DirectX::Keyboard> Input::m_keyboard = std::make_unique<DirectX::Keyboard>();
std::unique_ptr<DirectX::Mouse> Input::m_mouse = std::make_unique<DirectX::Mouse>();
std::unique_ptr<DirectX::Keyboard::KeyboardStateTracker> Input::m_keyboardState = std::make_unique<DirectX::Keyboard::KeyboardStateTracker>();
std::unique_ptr<DirectX::Mouse::ButtonStateTracker> Input::m_mouseState = std::make_unique<DirectX::Mouse::ButtonStateTracker>();

using namespace DirectX;

void Input::Update() {
	auto mouseState = Mouse::Get().GetState();
	m_mouseState->Update(mouseState);

	auto keyboardState = Keyboard::Get().GetState();
	m_keyboardState->Update(keyboardState);
}

bool Input::IsKeyDown(const Keyboard::Keys &p_key) {
	return m_keyboardState->IsKeyPressed(p_key);
}

bool Input::IsKeyDownOnce(const Keyboard::Keys &p_key) {
	return (m_keyboardState->IsKeyPressed(p_key) && !m_keyboardState->lastState.IsKeyDown(p_key));
}

bool Input::IsMouseButtonDown(const Mouse::Button &p_button) {
	switch (p_button) {
		case Mouse::Button::LeftButton:
		{
			return m_mouseState->leftButton == m_mouseState->PRESSED || m_mouseState->leftButton == m_mouseState->HELD;
			break;
		}

		case Mouse::Button::MiddleButton:
		{
			return m_mouseState->middleButton == m_mouseState->PRESSED || m_mouseState->middleButton == m_mouseState->HELD;
			break;
		}

		case Mouse::Button::RightButton:
		{
			return m_mouseState->rightButton == m_mouseState->PRESSED || m_mouseState->middleButton == m_mouseState->HELD;
			break;
		}

		case Mouse::Button::XButton1:
		{
			return m_mouseState->xButton1 == m_mouseState->PRESSED || m_mouseState->xButton1 == m_mouseState->HELD;
			break;
		}

		case Mouse::Button::XButton2:
		{
			return m_mouseState->xButton2 == m_mouseState->PRESSED || m_mouseState->xButton2 == m_mouseState->HELD;
			break;
		}
	}

	return false;
}

bool Input::IsMouseButtonDownOnce(const DirectX::Mouse::Button &p_button) {
	switch (p_button) {
		case Mouse::Button::LeftButton:
		{
			return m_mouseState->leftButton == m_mouseState->PRESSED;
			break;
		}

		case Mouse::Button::MiddleButton:
		{
			return m_mouseState->middleButton == m_mouseState->PRESSED;
			break;
		}

		case Mouse::Button::RightButton:
		{
			return m_mouseState->rightButton == m_mouseState->PRESSED;
			break;
		}

		case Mouse::Button::XButton1:
		{
			return m_mouseState->xButton1 == m_mouseState->PRESSED;
			break;
		}

		case Mouse::Button::XButton2:
		{
			return m_mouseState->xButton2 == m_mouseState->PRESSED;
			break;
		}
	}

	return false;
}