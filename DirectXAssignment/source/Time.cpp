#pragma once
#include "Time.h"
#include <chrono>

using namespace std::chrono;

time_point<high_resolution_clock> GameTime::m_startTime = high_resolution_clock::now();
time_point<high_resolution_clock> GameTime::m_previousFrame = high_resolution_clock::now();
time_point<high_resolution_clock> GameTime::m_currentFrame = high_resolution_clock::now();
double GameTime::m_deltaTime;

GameTime::GameTime() {
	m_startTime = high_resolution_clock::now();
}

void GameTime::Update() {
	m_previousFrame = m_currentFrame;
	m_currentFrame = high_resolution_clock::now();

	duration<double> dtDuration = duration_cast<microseconds>(m_currentFrame - m_previousFrame);
	m_deltaTime = dtDuration.count();
}

double GameTime::GetRunningTime() {
	duration<double> duration = duration_cast<seconds>(high_resolution_clock::now() - m_startTime);
	return duration.count();
}
