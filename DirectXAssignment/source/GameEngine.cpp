#include "GameEngine.h"
#include "Time.h"
#include "GraphicsEngine.h"
#include "EntityManager.h"
#include "Input.h"
#include "Player.h"

#include <iostream>

GameEngine::GameEngine(GraphicsEngine* p_graphicsEngine) {
	m_graphicsEngine = p_graphicsEngine;
	m_entityManager = new EntityManager();

	m_playerEntity = m_entityManager->CreateEntity<Player>();
	m_playerEntity->SetSprite(new Sprite(m_graphicsEngine->GetTexture("resources/fish.jpg")));
}

GameEngine::~GameEngine() {
	m_graphicsEngine = nullptr;

	if (m_entityManager != nullptr)
		delete m_entityManager;
}

void GameEngine::Update() {
	GameTime::Update();

	if (Input::IsKeyDown(DirectX::Keyboard::Keys::Space)) {
		std::cout << "ButtonDown!" << std::endl;
	}

	m_entityManager->UpdateAllEntities();
	m_entityManager->QueueAllEntitiesForDrawing(m_graphicsEngine);

}
