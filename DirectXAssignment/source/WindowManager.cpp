#if (DEBUG) || (_DEBUG)				//https://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k%28C4996%29&rd=true
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "WindowManager.h"
#include <iostream>
#include "Keyboard.h"
#include "Mouse.h"


HWND WindowManager::m_windowHandle;
RECT WindowManager::m_windowRect;
HINSTANCE WindowManager::m_instance;

WindowManager::WindowManager(HINSTANCE p_instance) {
#if (DEBUG) || (_DEBUG)
	AllocConsole();
	BindStdHandlesToConsole();
#endif
	
	m_instance = p_instance;

	WNDCLASS wc = { 0 };
	wc.lpfnWndProc = WindowManager::WindowProc;
	wc.hInstance = m_instance;
	wc.lpszClassName = L"D3D11";

	RegisterClass(&wc);

	DWORD windowStyle = WS_OVERLAPPEDWINDOW;
	RECT windowSize = { 1, 1, 800, 600 };
	m_windowRect = windowSize;		//Copy this here, to use later

	AdjustWindowRect(&windowSize, windowStyle, false);
	m_windowHandle = CreateWindow(L"D3D11", L"D3D11Init", windowStyle, CW_USEDEFAULT, CW_USEDEFAULT,
		windowSize.right - windowSize.left, windowSize.bottom - windowSize.top,
		0, 0, m_instance, 0);

	if (!m_windowHandle) {
		DWORD error = GetLastError();
		std::cout << "Failed to create handle, error: " << error << std::endl;
		PostQuitMessage(-1);
		return;
	}

	UpdateWindow(m_windowHandle);
	ShowWindow(m_windowHandle, SW_SHOW);
}

WindowManager::~WindowManager() {
	if (m_windowHandle != NULL)
		CloseWindow(m_windowHandle);

#if (DEBUG) || (_DEBUG)
	FreeConsole();
#endif

	UnregisterClass(L"D3D11", m_instance);
}

HWND WindowManager::GetWindowHandle() {
	return m_windowHandle;
}

RECT WindowManager::GetWindowRect() {
	return m_windowRect;
}

LRESULT CALLBACK WindowManager::WindowProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_PAINT:
			ValidateRect(window, 0);
			return 0;

		case WM_ACTIVATEAPP:
		{
			DirectX::Keyboard::ProcessMessage(message, wParam, lParam);
			DirectX::Mouse::ProcessMessage(message, wParam, lParam);
			break;
		}

		case WM_INPUT:
		case WM_MOUSEMOVE:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
		case WM_MOUSEWHEEL:
		case WM_MOUSEHOVER:
		{
			DirectX::Mouse::ProcessMessage(message, wParam, lParam);
			break;
		}

		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
		case WM_KEYUP:
		case WM_SYSKEYUP:
		{
			DirectX::Keyboard::ProcessMessage(message, wParam, lParam);
			break;
		}
	}

	return DefWindowProc(window, message, wParam, lParam);
}

//http://stackoverflow.com/a/25927081
void WindowManager::BindStdHandlesToConsole() {			
	// Redirect the CRT standard input, output, and error handles to the console
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);

	//Clear the error state for each of the C++ standard stream objects. We need to do this, as
	//attempts to access the standard streams before they refer to a valid target will cause the
	//iostream objects to enter an error state. In versions of Visual Studio after 2005, this seems
	//to always occur during startup regardless of whether anything has been read from or written to
	//the console or not.
	std::wcout.clear();
	std::cout.clear();
	std::wcerr.clear();
	std::cerr.clear();
	std::wcin.clear();
	std::cin.clear();
}