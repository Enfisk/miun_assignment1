#include "Texture.h"
#include "Utilities.h"
#include "WICTextureLoader.h"
#include <iostream>

Texture::Texture(const std::string &p_filePath, ID3D11Device *p_device) : m_filePath(p_filePath) {
	HRESULT hr = DirectX::CreateWICTextureFromFile(p_device, std::wstring(p_filePath.begin(), p_filePath.end()).c_str(), &m_texture, &m_srv);
	if (FAILED(hr)) {
		DWORD error = GetLastError();
		std::cout << "Texture::CreateWICTexture failed: " << error << std::endl;
		return;
	}
	
	ID3D11Texture2D *tex2D;
	hr = m_texture->QueryInterface(IID_ID3D11Texture2D, (void**)&tex2D);

	if (FAILED(hr)) {
		DWORD error = GetLastError();
		std::cout << "Texture::QueryInterface failed: " << error << std::endl;
		return;
	}

	tex2D->GetDesc(&m_desc);
}

Texture::~Texture() {
	if (m_texture != nullptr) {
		m_texture->Release();
		m_texture = nullptr;
	}
}

std::string Texture::GetFilepath() const {
	return m_filePath;
}

long Texture::GetWidth() const {
	return m_desc.Width;
}

long Texture::GetHeight() const {
	return m_desc.Height;
}
