#include "Player.h"
#include "Keyboard.h"	//DirectXTK <3

Player::Player() : Entity() {
	m_direction = MovementDirection::None;
}

Player::Player(const Vector2 &p_position) : Entity(p_position) {
	m_direction = MovementDirection::None;		//Wait until we get input to start moving
}

Player::~Player() {

}

void Player::Update() {

}
