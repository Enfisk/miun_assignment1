#pragma once
#include <string>
#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")

class Texture {
public:
	Texture(const std::string &p_filePath, ID3D11Device *p_device);
	~Texture();

	std::string GetFilepath() const;
	long GetWidth() const;
	long GetHeight() const;

	inline ID3D11Resource* GetTextureResource() const { return m_texture; };
	inline D3D11_TEXTURE2D_DESC GetTextureDescription() const { return m_desc; };
	inline ID3D11ShaderResourceView* GetShaderResourceView() const { return m_srv; };

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

private:
	const std::string m_filePath;
	ID3D11Resource *m_texture;
	ID3D11ShaderResourceView *m_srv;
	D3D11_TEXTURE2D_DESC m_desc;
};