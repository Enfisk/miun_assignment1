#pragma once
#include "SimpleMath.h"
#include "Texture.h"

using namespace DirectX::SimpleMath;

class Sprite {
public:
	Sprite(Texture* p_texture, Vector3 p_position = Vector3(0, 0, 0), Vector2 p_size = Vector2(0, 0)) {
		m_position = p_position;
		m_texture = p_texture;
		
		if (p_size == Vector2(0, 0)) {
			m_size = Vector2((float)m_texture->GetWidth(), (float)m_texture->GetHeight());
		}
		else {
			m_size = p_size;
		}

		m_textureRect.left = 0;
		m_textureRect.right = m_texture->GetWidth();
		m_textureRect.top = 0;
		m_textureRect.bottom = m_texture->GetHeight();
	}

	~Sprite() {};

	inline void SetPosition(const Vector3 &p_position) { m_position = p_position; }
	inline void SetSize(const Vector2 &p_size) { m_size = p_size; }
	inline void SetTextureRect(const RECT &p_rect) { m_textureRect = p_rect; }
	inline void SetTexture(Texture *p_texture) { m_texture = p_texture; }

	inline Vector3& GetPosition()	{ return m_position; }
	inline Vector2& GetSize()		{ return m_size; }
	inline RECT& GetTectureRect()	{ return m_textureRect; }
	inline Texture* GetTexture()	{ return m_texture; }

private:
	Vector3 m_position;		//X, Y, Depth
	Vector2 m_size;			//Width, height
	RECT m_textureRect;		//Where on the spritesheet are we

	Texture *m_texture;		//Don't delete this, it's handled by GraphicsEngine.
};