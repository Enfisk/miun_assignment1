#pragma once
#include "SimpleMath.h"
#include "Sprite.h"

using namespace DirectX::SimpleMath;


class Entity {
public:
	Entity(const Vector2 p_position = Vector2(0, 0)) { m_position = p_position; }
	virtual ~Entity() { if (m_sprite != nullptr) delete m_sprite; };

	Vector2 GetPosition() const { return m_position; }
	Sprite* GetSprite() const { return m_sprite; }

	void SetPosition(const Vector2 &p_position) { m_position = p_position; }
	void SetSprite(Sprite *p_sprite) { m_sprite = p_sprite; }

	virtual void Update() = 0;

private:
	Vector2 m_position;
	Sprite* m_sprite = nullptr;
};