#pragma once
#include "Keyboard.h"
#include "Mouse.h"
#include <memory>

class Input {
public:
	static void Update();

	static bool IsKeyDown(const DirectX::Keyboard::Keys &p_key);
	static bool IsKeyDownOnce(const DirectX::Keyboard::Keys &p_key);

	static bool IsMouseButtonDown(const DirectX::Mouse::Button &p_button);
	static bool IsMouseButtonDownOnce(const DirectX::Mouse::Button &p_button);

private:
	static std::unique_ptr<DirectX::Keyboard::KeyboardStateTracker> m_keyboardState;
	static std::unique_ptr<DirectX::Mouse::ButtonStateTracker> m_mouseState;
	static std::unique_ptr<DirectX::Mouse> m_mouse;
	static std::unique_ptr<DirectX::Keyboard> m_keyboard;
};