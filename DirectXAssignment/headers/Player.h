#pragma once
#include "Entity.h"

class Sprite;

class Player : public Entity {
	enum class MovementDirection {
		None,
		Up,
		Down,
		Left,
		Right
	};

public:
	Player();
	Player(const Vector2 &p_position);
	~Player();

	void Update();

private:
	MovementDirection m_direction;
	//Should probably have an animated sprite here or something
};