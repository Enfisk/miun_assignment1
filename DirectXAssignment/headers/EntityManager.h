#pragma once
#include <vector>
#include <memory>

class Entity;
class GraphicsEngine;

class EntityManager {
public:
	EntityManager();
	~EntityManager();

	//Some way to create an entity, and some way to remove entities

	void UpdateAllEntities();
	void QueueAllEntitiesForDrawing(GraphicsEngine *p_graphicsEngine);

	void RemoveEntity(Entity* p_entity);

	EntityManager(const EntityManager&) = delete;
	EntityManager& operator=(const EntityManager&) = delete;

	//I dislike doing this with raw pointers, but I can't figure out another way of doing it...
	template<typename T>
	Entity* CreateEntity() {
		Entity* object = new T();
		m_entities.push_back(object);
		return object;
	}

private:
	std::vector<Entity*> m_entities;
	std::vector<Entity*> m_entitiesToRemove;

	void DeleteAllQueuedEntities();
};