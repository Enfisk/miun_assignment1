#pragma once
#include <string>
#include <d3d11.h>
#include <map>
#include <vector>
#pragma comment(lib, "d3d11.lib")

namespace DirectX {
	class SpriteBatch;
	class CommonStates;
}

class Texture;
class Sprite;

class GraphicsEngine {
public:
	GraphicsEngine();
	~GraphicsEngine();

	void Draw();
	void QueueForDrawing(Sprite* p_drawable);

	Texture* GetTexture(const std::string &p_filePath);

private:
	bool CreateDeviceAndSwapChain();
	bool CreateRenderTargetView();
	bool CreateDepthStencil();
	void CreateViewport();

private:
	//Bryter det mot n�gra stilregler eller n�t att assignea pekare s�h�r?

	ID3D11Device *m_device = nullptr;
	ID3D11DeviceContext *m_deviceContext = nullptr;
	IDXGISwapChain *m_swapChain = nullptr;

	ID3D11RenderTargetView *m_renderTargetView = nullptr;

	D3D11_TEXTURE2D_DESC m_backBufferDesc;
	ID3D11Texture2D *m_depthStencilBuffer = nullptr;
	ID3D11DepthStencilView *m_depthStencilView = nullptr;

	D3D11_VIEWPORT m_viewPort;

	D3D_FEATURE_LEVEL m_featureLevel;

	std::map<std::string, Texture*> m_textures;
	std::vector<Sprite*> m_queuedDrawables;
	DirectX::SpriteBatch* m_spriteBatch;
	DirectX::CommonStates* m_states;
};