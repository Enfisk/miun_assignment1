#pragma once
#include "Windows.h"
#include <iostream>
#include <cmath>

namespace Utilities {
	static inline long GetRectWidth(RECT p_rect) {
		return p_rect.right - p_rect.left;
	}

	static inline long GetRectHeight(RECT p_rect) {
		return p_rect.bottom - p_rect.top;
	}

	static inline RECT GetWindowRect(HWND p_windowHandle) {
		RECT returnVal = { 0 };
		GetClientRect(p_windowHandle, &returnVal);
		return returnVal;
	}

	static inline DXGI_FORMAT ComponentsToFormat(const int &p_components) {
		switch (p_components) {
			case 1:
				return DXGI_FORMAT_R8_UNORM;
				break;

			case 2:
				return DXGI_FORMAT_R8G8_UNORM;
				break;

			case 3:
				return DXGI_FORMAT_R8G8B8A8_UNORM;		//this might bite me in the ass later...
				break;

			case 4:
				return DXGI_FORMAT_R8G8B8A8_UNORM;
				break;

			default:
				std::cout << "Invalid component \'" << p_components << "\'!" << std::endl;
				return DXGI_FORMAT_UNKNOWN;
				break;
		}
	}

	//struct Vector2 {
	//public:
	//	Vector2() { x = 0; y = 0; }
	//	Vector2(float p_x, float p_y) { x = p_x; y = p_y; }
	//	Vector2(int p_x, int p_y) { x = static_cast<float>(p_x); y = static_cast<float>(p_y); }

	//	float GetLength() const { return sqrtf(x*x + y*y); }

	//	Vector2 operator=(const Vector2 &p_rhs) { this->x = p_rhs.x; this->y = p_rhs.y; return *this; }
	//	Vector2 operator+(const Vector2 &p_rhs) { return Vector2(this->x + p_rhs.x, this->y + p_rhs.y); }
	//	Vector2 operator-(const Vector2 &p_rhs) { return Vector2(this->x - p_rhs.x, this->y - p_rhs.y); }
	//	Vector2 operator+=(const Vector2 &p_rhs) { this->x += p_rhs.x; this->y += p_rhs.y; return *this; }
	//	Vector2 operator-=(const Vector2 &p_rhs) { this->x -= p_rhs.x; this->y -= p_rhs.y; return *this; }
	//	bool operator==(const Vector2 &p_rhs) { return (this->x == p_rhs.x && this->y == p_rhs.y); }
	//	bool operator!=(const Vector2 &p_rhs) { return (this->x != p_rhs.x || this->y != p_rhs.y); }
	//	bool operator<(const Vector2 &p_rhs) { return this->GetLength() < p_rhs.GetLength(); }
	//	bool operator>(const Vector2 &p_rhs) { return this->GetLength() > p_rhs.GetLength(); }
	//	bool operator<=(const Vector2 &p_rhs) { return this->GetLength() <= p_rhs.GetLength(); }
	//	bool operator>=(const Vector2 &p_rhs) { return this->GetLength() >= p_rhs.GetLength(); }

	//	float x, y;
	//};

	//typedef Vector2 Vertex;
}