#pragma once
#include <chrono>

using namespace std::chrono;

class GameTime {
public:
	GameTime();

	static void Update();

	inline static double GetDeltaTime() { return m_deltaTime; }
	inline static time_point<high_resolution_clock> GetStartTime() { return m_startTime; }
	inline static double GetRunningTime();

private:
	static time_point<high_resolution_clock> m_startTime;
	static time_point<high_resolution_clock> m_previousFrame;
	static time_point<high_resolution_clock> m_currentFrame;
	static double m_deltaTime;
};