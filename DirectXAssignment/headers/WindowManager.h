#pragma once
#include <Windows.h>

class WindowManager {
public:
	WindowManager(HINSTANCE p_instance);
	~WindowManager();

	static LRESULT CALLBACK WindowProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam);

	static HWND GetWindowHandle();
	static RECT GetWindowRect();
private:
	void BindStdHandlesToConsole();			//http://stackoverflow.com/a/25927081

	static HWND m_windowHandle;		//We will most likely only use one window.
	static RECT m_windowRect;
	static HINSTANCE m_instance;
};