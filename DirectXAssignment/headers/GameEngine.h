#pragma once

class GraphicsEngine;
class EntityManager;
class Entity;

class GameEngine {
public:
	GameEngine(GraphicsEngine* p_graphicsEngine);
	~GameEngine();

	void Update();

	GameEngine(const GameEngine&) = delete;
	GameEngine& operator=(const GameEngine&) = delete;

private:
	GraphicsEngine* m_graphicsEngine;		//Gets deleted by Engine.h
	EntityManager* m_entityManager;
	Entity* m_playerEntity;
};