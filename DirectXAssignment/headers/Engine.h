#pragma once
#include <Windows.h>

class GraphicsEngine;
class WindowManager;
class GameEngine;

class Engine {
public:
	Engine(HINSTANCE p_instance);
	~Engine();

	Engine& operator=(const Engine&) = delete;
	Engine(const Engine&) = delete;

	void Run();
private:
	void PollEvents();

private:
	bool m_running = true;
	unsigned int m_sleepDuration = 10;

	GraphicsEngine* m_graphics;
	WindowManager* m_windowManager;
	GameEngine* m_gameEngine;
};